package com.example.mycustomcalendar;

// import libraries
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;

// Klasse MainActivity erbt von AppCompatActivity
public class MainActivity extends AppCompatActivity {
//Deklarartion der Attribute
    private mySQLiteDBHandler dbHandler;
    private EditText editText;
    private CalendarView calendarView;
    private String selectedDate;
    private SQLiteDatabase sqlLiteDatabase;
    private Button button;



// Überschreiben der OnCreate Methode
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity2();
            }
        });


// Zuordnung der jeweiligen Attribute mit der ID
        editText = findViewById(R.id.editText);
        calendarView = findViewById(R.id.calendarView);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            //Methode zum Auswählen des Datums in der App
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
            //Zuordnung der Variablen selectedDate
                selectedDate = Integer.toString(year) + Integer.toString(month) + Integer.toString(dayOfMonth);
                //Lesen der Daten
                ReadDatabase(view);
            }
        });
//Try-Catch Befehl damit komplette App nicht abstürzt wenn ein Fehler auftritt
        try{
        // Erzeugung/Instanziierung einer Datenbank
            dbHandler = new mySQLiteDBHandler(this, "CalendarDatabase", null, 1);
            sqlLiteDatabase = dbHandler.getWritableDatabase();
            //Anlegen einer Datenbank/Tabelle mit der Bezeichnung EventCalendar
            sqlLiteDatabase.execSQL("CREATE TABLE EventCalendar(Date TEXT, Event TEXT)");
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void openActivity2() {
        Intent intent = new Intent (this, Activity2.class);
        startActivity(intent);
    }

    //Methode für Eingabe der Daten im Kalender
    public void InsertDatabase(View view){
        ContentValues contentValues = new ContentValues();
        contentValues.put("Date", selectedDate);
        contentValues.put("Event", editText.getText().toString());
        sqlLiteDatabase.insert("EventCalendar", null, contentValues);


    }
    //Auslesen der eingegebenen Daten
    public void ReadDatabase(View view){
        String query = "Select Event from EventCalendar where Date = " + selectedDate;
        try{
            Cursor cursor = sqlLiteDatabase.rawQuery(query, null);
            cursor.moveToFirst();
            editText.setText(cursor.getString(0));
        }
        catch (Exception e){
            e.printStackTrace();
            editText.setText("");

        }
    }
    //Methode zum löschen der eingegebenen Daten in der Datenbank
    public void DeleteEvent(View view) {
        String query = "Delete from EventCalendar where Date = " + selectedDate;
        try {
            Cursor cursor = sqlLiteDatabase.rawQuery(query, null);
            cursor.moveToFirst();
            editText.setText(cursor.getString(0));
            //sqlLiteDatabase.delete(EventCalendar, Event, Date where Date = selectedDate);

        } catch (Exception e) {
            e.printStackTrace();


        }
    }
}
