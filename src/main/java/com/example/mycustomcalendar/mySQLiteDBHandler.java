package com.example.mycustomcalendar;

//importieren libraries
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

//Klasse mySQLiteDBHandler erbt von SQLiteOpenHelper(diese �bernimmt die Kommunikation mit der SQLite Datenbank)
public class mySQLiteDBHandler extends SQLiteOpenHelper {
//Konstruktor des mySQLiteDBHandler
    public mySQLiteDBHandler(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
